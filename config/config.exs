# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :snorlex,
  ecto_repos: [Snorlex.Repo]

# Configures the endpoint
config :snorlex, SnorlexWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "SPJSk5IAft7dii2syHSJDmCySi6Vbop6Oi1YZO9vuaB5JogpqMUx9MsNQ5ybPjtO",
  render_errors: [view: SnorlexWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Snorlex.PubSub,
  live_view: [signing_salt: "tIxZoy1v"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
