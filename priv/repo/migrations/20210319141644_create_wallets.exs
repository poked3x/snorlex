defmodule Snorlex.Repo.Migrations.CreateWallets do
  use Ecto.Migration

  def change do
    create table(:wallets) do
      add :address, :string

      timestamps()
    end

  end
end
