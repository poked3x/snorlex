defmodule Snorlex.Repo do
  use Ecto.Repo,
    otp_app: :snorlex,
    adapter: Ecto.Adapters.Postgres
end
